import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { AuthModule } from './auth/auth.module';
import { CoursesModule } from './courses/courses.module';
import { StudentsModule } from './students/students.module';
import { LabsModule } from './labs/labs.module';
import { LogbookModule } from './logbook/logbook.module';

@Module({
  imports: [ConfigModule.forRoot({isGlobal: true}), PrismaModule, AuthModule, CoursesModule, StudentsModule, LabsModule, LogbookModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
