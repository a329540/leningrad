import { ForbiddenException, Injectable } from '@nestjs/common';
import { LoginDto, SignupDto } from './dtos';
import * as argon from 'argon2';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class AuthService {
    constructor(private prismaService: PrismaService) {}

    async signup(signupDto: SignupDto) {
        const hash = await argon.hash(signupDto.password);
        var user = await this.prismaService.user.findUnique({
            where: {
                email: signupDto.email
            }
        });
        if(user)
            throw new ForbiddenException('email already registered');
        user = await this.prismaService.user.create({
            data: {
                email: signupDto.email,
                password: hash,
                name: signupDto.name
            }
        });
        delete user.password;
        return user;
    }

    async login(loginDto: LoginDto) {
        const user = await this.prismaService.user.findUnique({
            where: {
                email: loginDto.email
            }
        });
        if(!user || !(await argon.verify(user.password, loginDto.password)))
            throw new ForbiddenException('invalid credentials')
        delete user.password;
        return user;
    }
}
