import { Body, Controller, Post } from '@nestjs/common';
import { CoursesService } from './courses.service';
import { UploadDto } from './dtos';

@Controller('courses')
export class CoursesController {
    constructor(private coursesService: CoursesService) {}

    @Post()
    upload(@Body() uploadDto: UploadDto) {
        return this.coursesService.upload(uploadDto);
    }
}
