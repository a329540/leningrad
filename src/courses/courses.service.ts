import { Injectable } from '@nestjs/common';
import { Class, Professor } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { UploadDto } from './dtos';

@Injectable()
export class CoursesService {
    constructor(private prismaService: PrismaService) {}

    async upload(uploadDto: UploadDto) {
        await this.prismaService.log.deleteMany({});
        await this.prismaService.course.deleteMany({});
        await this.prismaService.class.deleteMany({});
        await this.prismaService.professor.deleteMany({});

        var classes: Class[] = [];
        var professors: Professor[] = [];
        var courses: {
            name: string,
            class_id: string,
            professor_id: number,
            key: string
        }[] = [];
        uploadDto.Cursos.forEach(course => {
            classes.push({id: course.Grupo});
            professors.push({id: course.Cve_Empleado, name: course.Nombre});
            courses.push({
                name: course.Materia,
                class_id: course.Grupo,
                professor_id: course.Cve_Empleado,
                key: course.Clave.toString()
            });
        });
        await this.prismaService.class.createMany({
            data: classes,
            skipDuplicates: true
        });
        await this.prismaService.professor.createMany({
            data: professors,
            skipDuplicates: true
        });
        await this.prismaService.course.createMany({
            data: courses
        });
    }
}
