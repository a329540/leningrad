import { IsArray } from "class-validator";

export class UploadDto {
    @IsArray()
    Cursos: {
        Grupo: string,
        Clave: any,
        Materia: string,
        Cve_Empleado: number,
        Nombre: string
    }[];
}
