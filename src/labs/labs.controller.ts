import { Body, Controller, Delete, Param, Post } from '@nestjs/common';
import { CreateDto } from './dtos';
import { LabsService } from './labs.service';

@Controller('labs')
export class LabsController {
    constructor(private labsService: LabsService) {}

    @Post()
    create(@Body() createDto: CreateDto) {
        return this.labsService.create(createDto);
    }

    @Delete(':id')
    destroy(@Param('id') id) {
        return this.labsService.destroy(parseInt(id));
    }
}
