import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateDto } from './dtos';

@Injectable()
export class LabsService {
    constructor(private prismaService: PrismaService) {}

    async create(createDto: CreateDto) {
        return await this.prismaService.lab.create({
            data: {
                name: createDto.name
            }
        });
    }

    async destroy(id: number) {
        try {
            return await this.prismaService.lab.delete({
                where: {
                    id: id
                }
            });
        } catch(error) {
            if(error instanceof PrismaClientKnownRequestError) {
                switch(error.code) {
                    case "P2025":
                        return null;
                    case "P2003":
                        throw new ForbiddenException("Foreign key constraint failed");
                }
            }
            throw error;
        }
    }
}
