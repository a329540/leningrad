import { IsISO8601 } from "class-validator";

export class GetReportDto {
    @IsISO8601()
    init: Date;
    @IsISO8601()
    end: Date;
}
