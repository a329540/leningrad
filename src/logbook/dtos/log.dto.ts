import { IsISO8601, IsNumber } from "class-validator";

export class LogDto {
    @IsISO8601()
    timestamp: Date;
    @IsNumber()
    course_id: number;
    @IsNumber()
    lab_id: number;
    @IsNumber()
    student_id: number;
}
