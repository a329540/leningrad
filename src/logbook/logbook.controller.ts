import { Body, Controller, Get, HttpCode, HttpStatus, Post } from '@nestjs/common';
import { GetReportDto, LogDto } from './dtos';
import { LogbookService } from './logbook.service';

@Controller('logbook')
export class LogbookController {
    constructor(private logbookService: LogbookService) {}

    @Post()
    log(@Body() logDto: LogDto) {
        return this.logbookService.log(logDto);
    }

    @HttpCode(HttpStatus.OK)
    @Get()
    getReport(@Body() getReportdto: GetReportDto) {
        return this.logbookService.getReport(getReportdto);
    }
}
