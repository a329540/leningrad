import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';
import { PrismaService } from 'src/prisma/prisma.service';
import { GetReportDto, LogDto } from './dtos';

@Injectable()
export class LogbookService {
    constructor(private prismaService: PrismaService) {}

    async log(logDto: LogDto) {
        try {
            return await this.prismaService.log.create({
                data: {
                    timestamp: logDto.timestamp,
                    course_id: logDto.course_id,
                    lab_id: logDto.lab_id,
                    student_id: logDto.student_id
                }
            });
        } catch(error) {
            if(error instanceof PrismaClientKnownRequestError) {
                if(error.code === 'P2003')
                throw new BadRequestException('Foreign key constraint failed');
            }
            throw error;
        }
    }

    async getReport(getReportDto: GetReportDto) {
        return await this.prismaService.log.findMany({
            where: {
                timestamp: {
                    gte: getReportDto.init,
                    lte: getReportDto.end
                }
            },
            select: {
                timestamp: true,
                course: true,
                student: true
            }
        });
    }
}
