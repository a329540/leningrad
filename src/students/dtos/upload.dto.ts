import { IsArray } from "class-validator";

export class UploadDto {
    @IsArray()
    Estudiantes: {
        Matricula: number,
        NombredelAlumno: string
    }[];
}