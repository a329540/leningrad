import { Body, Controller, Post } from '@nestjs/common';
import { UploadDto } from './dtos';
import { StudentsService } from './students.service';

@Controller('students')
export class StudentsController {
    constructor(private studentsService: StudentsService) {}

    @Post()
    upload(@Body() uploadDto: UploadDto) {
        return this.studentsService.upload(uploadDto);
    }
}
