import { Injectable } from '@nestjs/common';
import { Student } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { UploadDto } from './dtos';

@Injectable()
export class StudentsService {
    constructor(private prismaService: PrismaService) {}

    async upload(uploadDto: UploadDto) {
        var students: Student[] = [];
        uploadDto.Estudiantes.forEach(student => {
            students.push({
                id: student.Matricula,
                name: student.NombredelAlumno
            });
        });
        await this.prismaService.student.createMany({
            data: students,
            skipDuplicates: true
        });
    }
}
